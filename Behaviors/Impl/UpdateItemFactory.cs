﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Behaviors.Impl
{
    public class UpdateItemFactory : IUpdateItemFactory
    {
        #region Singleton
        public static UpdateItemFactory instance;

        public static UpdateItemFactory GetInstance()
        {
            if (instance == null)
            {
                instance = new UpdateItemFactory();
            }
            return instance;
        }
        #endregion

        private IDictionary<UpdateItemStrategyEnum, IUpdateItem> _behaviors;

        private UpdateItemFactory()
        {
            _behaviors = new Dictionary<UpdateItemStrategyEnum, IUpdateItem>();
        }

        public IUpdateItem GetBehavior(UpdateItemStrategyEnum updateFactoryStrategy)
        {
            IUpdateItem response = null;
            if (_behaviors.ContainsKey(updateFactoryStrategy))
            {
                response = _behaviors[updateFactoryStrategy];
            }
            if (response == null)
            {
                switch (updateFactoryStrategy)
                {
                    case UpdateItemStrategyEnum.NormalQuantity:
                        response = new NormalQualityBehavior();
                        break;
                    case UpdateItemStrategyEnum.NormalSellIn:
                        response = new NormalSellInBehavior();
                        break;
                    case UpdateItemStrategyEnum.AgedBrieQuality:
                        response = new AgedBrieQualityBehavior();
                        break;
                    case UpdateItemStrategyEnum.Sulfuras:
                        response = new SulfurasItemBehavior();
                        break;
                    case UpdateItemStrategyEnum.BackStageQuality:
                        response = new BackStageQualityBehavior();
                        break;
                    default:
                        response = new NormalQualityBehavior();
                        break;
                }
                _behaviors.Add(updateFactoryStrategy, response);
            }            
            return response;
        }
    }
}
