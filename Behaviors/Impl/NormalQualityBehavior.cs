﻿using csharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Behaviors.Impl
{
    public class NormalQualityBehavior : IUpdateItem
    {
        public void Update(Item item)
        {
            item.Quality--;
            if (item.SellIn < ItemValueConst.LowestQuality)
            {
                item.Quality--;
            }
            if (item.Quality < ItemValueConst.LowestQuality)
            {
                item.Quality = ItemValueConst.LowestQuality;
            }           
        }
    }
}
