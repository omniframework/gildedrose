﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Behaviors.Impl
{
    public class NormalSellInBehavior : IUpdateItem
    {
        public void Update(Item item)
        {
            item.SellIn--;
        }
    }
}
