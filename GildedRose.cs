﻿using csharp.Behaviors;
using csharp.Behaviors.Impl;
using csharp.Enums;
using System.Collections.Generic;

namespace csharp
{
    public class GildedRose
    {
        IList<Item> Items;

        private readonly IUpdateItemFactory _updateItemFactory;       

        public GildedRose(IList<Item> Items)
        {
            this.Items = Items;
            this._updateItemFactory = UpdateItemFactory.GetInstance();
        }

        public void UpdateQuality()
        {
            foreach (var item in Items)
            {
                switch (item.Name)
                {
                    case ItemNames.AgedBrie:
                        ApplyBehavior(item, UpdateItemStrategyEnum.AgedBrieQuality);
                        ApplyBehavior(item, UpdateItemStrategyEnum.NormalSellIn);
                        break;
                    case ItemNames.SulfurasHandRagnaros:
                        ApplyBehavior(item, UpdateItemStrategyEnum.Sulfuras);
                        break;
                    case ItemNames.BackstagePassesTAFKAL80ETCconcert:
                        ApplyBehavior(item, UpdateItemStrategyEnum.BackStageQuality);
                        ApplyBehavior(item, UpdateItemStrategyEnum.NormalSellIn);
                        break;
                    default:
                        ApplyBehavior(item, UpdateItemStrategyEnum.NormalQuantity);
                        ApplyBehavior(item, UpdateItemStrategyEnum.NormalSellIn);                        
                        break;
                }
            }
        }       

        public void ApplyBehavior(Item item, UpdateItemStrategyEnum updateItemStrategyEnum)
        {
            _updateItemFactory.GetBehavior(updateItemStrategyEnum).Update(item);
        }
    }
}
