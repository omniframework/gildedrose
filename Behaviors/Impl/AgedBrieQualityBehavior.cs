﻿using csharp.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp.Behaviors.Impl
{
    public class AgedBrieQualityBehavior : IUpdateItem
    {       
        public void Update(Item item)
        {
            item.Quality++;
            if (item.SellIn < ItemValueConst.LowestSellIn)
            {
                item.Quality++;
            }
            if (item.Quality > ItemValueConst.HigestQuality)
            {
                item.Quality = ItemValueConst.HigestQuality;
            }
        }
    }
}
